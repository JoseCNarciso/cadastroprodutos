import db.ProdutosDB;
import models.Produto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class Main {
    static ProdutosDB produtodDB = new ProdutosDB();
    public static void main( String[] args ) throws  Exception{

        System.out.println("----PEDIDO DE VENDAS----");
        int option;

        do {
            System.out.println("01- Cadastro de produto");
            System.out.println("0- Para sair");

            Scanner scanner = new Scanner(System.in);
            option = scanner.nextInt();

            System.out.println("Qual operação deseja realizar");

            process(option);
        } while (option != 0);


    }

    public static void process( int otpion ) throws Exception {
        switch (otpion) {
            case 1: {
                Scanner scanner = new Scanner(System.in);


                System.out.print("Qual a descrição você deseja dar ao novo protuto: ");
                String descricao = scanner.nextLine();
                System.out.println("Qual a ID você deseja dar ao novo produto: ");
                int id = scanner.nextInt();

                System.out.println("Qual o preço deseja cadastrar: ");
                double preco = scanner.nextDouble();

                System.out.println("Qual a data de validade: ");
                String dataString = scanner.next();

                Date dataValidade = new SimpleDateFormat("dd/MM/yyyy").parse(dataString);




                Produto novoProduto = new Produto(id, descricao,preco,dataValidade);


                System.out.println("Produto criado com sucesso: ");
                System.out.println("---ID: " + novoProduto.getId());
                System.out.println("---Descrição: " + novoProduto.getDescricao());
                System.out.println("---Preço: " + novoProduto.getPreco());
                System.out.println("---Data de validade do produto: " + novoProduto.getDataValidade());
                System.out.println("-------------------------------------------------");
            }

        }
    }
}
